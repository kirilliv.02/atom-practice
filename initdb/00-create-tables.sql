create table car
(
    id            bigserial,
    licence_plate varchar not null check ( length(licence_plate) >= 5 ) primary key,
    mileage       int check ( mileage >= 0 ),
    consumption   int check ( consumption >= 0 ),
    tax           int check ( tax >= 0 )
);

create table car_info
(
    id                  bigserial primary key not null,
    licence_plate       varchar(10) unique    not null check ( length(licence_plate) >= 5 ) references car (licence_plate),
    model               varchar(20)           not null,
    year_of_manufacture int                   not null check ( year_of_manufacture >= 2000 ),
    type_car            int                   not null
);