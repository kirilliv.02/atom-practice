### Запуск проекта
1. Склоинровать репоизиторий
    ```
    git clone https://gitlab.com/kirilliv.02/atom-practice.git
    ```
2. Запустить проект
    ```
    docker compose up --build -d
    ```
3. Запустить клиента
    ```
    docker exec -it client /main
    ```