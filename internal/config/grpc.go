package config

import (
	"Fleet_Management_System/internal/customError"
	"crypto/tls"
	"crypto/x509"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"net"
	"os"
	"time"
)

type grpcConfig struct {
	Host                  string
	Port                  string
	MaxReceiveMsgSize     int
	WriteBufferSize       int
	ConnectionTimeout     time.Duration
	InitialConnWindowSize int32
	InitialWindowSize     int32
	MaxConcurrentStreams  uint32
}

func SetupGrpc() (*grpc.Server, string, error) {
	config := &grpcConfig{}

	err := viper.UnmarshalKey("grpc", config)

	if err != nil {
		return nil, "", customError.Wrap("grpcConfig", "SetupGrpc", "failed to unmarshal config", customError.Internal, err)
	}

	creds, err := loadTLSCredentialsServer()

	if err != nil {
		return nil, "", customError.Wrap("grpcConfig", "SetupGrpc", "could not load TLS keys", customError.Internal, err)
	}

	opts := []grpc.ServerOption{
		grpc.Creds(creds),
		grpc.ConnectionTimeout(time.Minute * config.ConnectionTimeout),
		grpc.InitialConnWindowSize(config.InitialConnWindowSize << 23),
		grpc.InitialWindowSize(config.InitialWindowSize << 23),
		grpc.MaxConcurrentStreams(config.MaxConcurrentStreams),
		grpc.MaxRecvMsgSize(config.MaxReceiveMsgSize << 23),
		grpc.WriteBufferSize(config.WriteBufferSize << 23),
	}

	addr := net.JoinHostPort(config.Host, config.Port)

	return grpc.NewServer(opts...), addr, nil
}

func loadTLSCredentialsServer() (credentials.TransportCredentials, error) {
	pemClientCA, err := os.ReadFile("cert/ca-cert.pem")
	if err != nil {
		return nil, customError.Wrap("grpcConfig", "loadTLSCredentialsClient", "failed read cert", customError.Internal, err)
	}

	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(pemClientCA) {
		return nil, customError.Wrap("grpcConfig", "loadTLSCredentialsClient", "failed to add server CA's certificate", customError.Internal, err)
	}

	serverCert, err := tls.LoadX509KeyPair("./cert/server-cert.pem", "./cert/server-key.pem")
	if err != nil {
		return nil, customError.Wrap("grpcConfig", "loadTLSCredentialsClient", "failed to load X509 key pair", customError.Internal, err)
	}

	c := &tls.Config{
		Certificates: []tls.Certificate{serverCert},
		ClientAuth:   tls.RequireAndVerifyClientCert,
		ClientCAs:    certPool,
	}

	return credentials.NewTLS(c), nil
}
