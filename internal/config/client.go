package config

import (
	"Fleet_Management_System/internal/customError"
	"crypto/tls"
	"crypto/x509"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"net"
	"os"
)

type clientConfig struct {
	Host string
	Port string
}

func SetupClient() (*grpc.ClientConn, error) {
	config := &clientConfig{}

	err := viper.UnmarshalKey("client.grpc", config)

	if err != nil {
		return nil, customError.Wrap("clientConfig", "SetupClient", "failed to unmarshal config", customError.Internal, err)
	}

	creds, err := loadTLSCredentialsClient()
	if err != nil {
		return nil, customError.Wrap("clientConfig", "SetupClient", "could not load TLS keys", customError.Internal, err)
	}

	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(creds),
	}

	conn, err := grpc.Dial(net.JoinHostPort(config.Host, config.Port), opts...)

	if err != nil {
		return nil, customError.Wrap("clientConfig", "SetupClient", "failed to create grpc connection", customError.Internal, err)
	}

	return conn, err
}

func loadTLSCredentialsClient() (credentials.TransportCredentials, error) {
	pemServerCA, err := os.ReadFile("cert/ca-cert.pem")
	if err != nil {
		return nil, customError.Wrap("clientConfig", "loadTLSCredentialsClient", "failed read cert", customError.Internal, err)
	}

	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(pemServerCA) {
		return nil, customError.Wrap("clientConfig", "loadTLSCredentialsClient", "failed to add server CA's certificate", customError.Internal, err)
	}

	clientCert, err := tls.LoadX509KeyPair("cert/client-cert.pem", "cert/client-key.pem")
	if err != nil {
		return nil, customError.Wrap("clientConfig", "loadTLSCredentialsClient", "failed to load X509 key pair", customError.Internal, err)
	}

	config := &tls.Config{
		Certificates: []tls.Certificate{clientCert},
		RootCAs:      certPool,
	}

	return credentials.NewTLS(config), nil
}
