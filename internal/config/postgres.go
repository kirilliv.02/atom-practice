package config

import (
	"Fleet_Management_System/internal/customError"
	"context"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/spf13/viper"
	"time"
)

type postgresConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Name     string
	SslMode  string
}

func SetupPostgres(ctx context.Context) (*pgxpool.Pool, error) {
	config := &postgresConfig{}

	err := viper.UnmarshalKey("database.postgres", config)

	if err != nil {
		return nil, customError.Wrap("postgresConfig", "SetupPostgres", "failed to unmarshal config", customError.Internal, err)
	}

	pgxConn := fmt.Sprintf("postgresql://%s:%s@%s:%s/%s?sslmode=%s",
		config.User,
		config.Password,
		config.Host,
		config.Port,
		config.Name,
		config.SslMode)

	connConf, err := pgxpool.ParseConfig(pgxConn)

	if err != nil {
		return nil, customError.Wrap("postgresConfig", "SetupPostgres", "NewPostgresPool failed parse config", customError.Internal, err)
	}

	connConf.MaxConns = 10
	connConf.MinConns = 10
	connConf.MaxConnLifetime = 3 * time.Minute

	pool, err := pgxpool.NewWithConfig(ctx, connConf)

	if err != nil {
		return nil, customError.Wrap("postgresConfig", "SetupPostgres", "NewPostgresPool failed create pool", customError.Internal, err)
	}

	err = pool.Ping(ctx)

	if err != nil {
		return nil, customError.Wrap("postgresConfig", "SetupPostgres", "NewPostgresPool failed ping", customError.Internal, err)
	}

	return pool, nil
}
