package server

import (
	"Fleet_Management_System/internal/customError"
	"Fleet_Management_System/internal/model"
	pb "Fleet_Management_System/proto"
	"context"
	"fmt"
	"github.com/go-playground/validator/v10"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CarService interface {
	RegistrationNewCar(context.Context, *model.Car) (*model.Car, error)
	GetCarMileage(context.Context, string) (int32, error)
	GetCostOfCar(context.Context, string) (int32, error)
	GetCarInfo(context.Context, string) (*model.CarInfo, error)
	CreateCarInfo(context.Context, *model.CarInfo) (*model.CarInfo, error)
	UpdateCarInfo(context.Context, *model.CarInfo) (*model.CarInfo, error)
	DeleteCarInfo(context.Context, string) (*model.CarInfo, error)
	GetAllCarInfo(context.Context) ([]*model.CarInfo, error)
}

type CarServer struct {
	pb.CarInfoServiceServer
	carService CarService
}

func NewCarServer(carService CarService) *CarServer {
	return &CarServer{
		carService: carService,
	}
}

func (s *CarServer) GetCostOfCar(ctx context.Context, req *pb.CarInfoRequest) (*pb.CostOfCar, error) {
	costOfUse, err := s.carService.GetCostOfCar(ctx, req.LicencePlate)

	if err != nil {
		return nil, handleError(err)
	}

	return &pb.CostOfCar{CostOfUse: costOfUse}, nil
}
func (s *CarServer) GetCarMileage(ctx context.Context, req *pb.CarInfoRequest) (*pb.Mileage, error) {
	mileage, err := s.carService.GetCarMileage(ctx, req.LicencePlate)

	if err != nil {
		return nil, handleError(err)
	}

	return &pb.Mileage{Mileage: mileage}, nil
}
func (s *CarServer) RegistrationNewCar(ctx context.Context, req *pb.Car) (*pb.Car, error) {
	car := &model.Car{
		LicencePlate: req.LicencePlate,
		Mileage:      req.Mileage,
		Consumption:  req.Consumption,
		Tax:          req.Tax,
	}

	err := validation(car)

	if err != nil {
		return nil, err
	}
	car, err = s.carService.RegistrationNewCar(ctx, car)

	if err != nil {
		return nil, handleError(err)
	}

	return &pb.Car{
		LicencePlate: car.LicencePlate,
		Mileage:      car.Mileage,
		Consumption:  car.Consumption,
		Tax:          car.Tax,
	}, nil
}

func (s *CarServer) GetCarInfo(ctx context.Context, req *pb.CarInfoRequest) (*pb.CarInfo, error) {
	carInfo, err := s.carService.GetCarInfo(ctx, req.LicencePlate)

	if err != nil {
		return nil, handleError(err)
	}

	return &pb.CarInfo{
		LicencePlate:      carInfo.LicencePlate,
		Model:             carInfo.Model,
		YearOfManufacture: carInfo.YearOfManufacture,
		Type:              carInfo.TypeCar.String(),
	}, nil
}

func (s *CarServer) CreateCarInfo(ctx context.Context, carInfoReq *pb.CarInfo) (*pb.CarInfo, error) {
	carInfo := &model.CarInfo{
		LicencePlate:      carInfoReq.LicencePlate,
		Model:             carInfoReq.Model,
		YearOfManufacture: carInfoReq.YearOfManufacture,
	}

	carInfo.SetTypeCar(carInfoReq.Type)

	err := validation(carInfo)

	if err != nil {
		return nil, err
	}

	carInfo, err = s.carService.CreateCarInfo(ctx, carInfo)

	if err != nil {
		return nil, handleError(err)
	}

	return &pb.CarInfo{
		LicencePlate:      carInfo.LicencePlate,
		Model:             carInfo.Model,
		YearOfManufacture: carInfo.YearOfManufacture,
		Type:              carInfo.TypeCar.String(),
	}, nil
}

func (s *CarServer) UpdateCarInfo(ctx context.Context, req *pb.CarInfo) (*pb.CarInfo, error) {

	carInfo := &model.CarInfo{
		LicencePlate:      req.LicencePlate,
		Model:             req.Model,
		YearOfManufacture: req.YearOfManufacture,
	}

	carInfo.SetTypeCar(req.Type)

	err := validation(carInfo)

	if err != nil {
		return nil, err
	}

	carInfo, err = s.carService.UpdateCarInfo(ctx, carInfo)

	if err != nil {
		return nil, handleError(err)
	}

	return &pb.CarInfo{
		LicencePlate:      carInfo.LicencePlate,
		Model:             carInfo.Model,
		YearOfManufacture: carInfo.YearOfManufacture,
		Type:              carInfo.TypeCar.String(),
	}, nil
}
func (s *CarServer) DeleteCarInfo(ctx context.Context, req *pb.CarInfoRequest) (*pb.CarInfo, error) {
	carInfo, err := s.carService.DeleteCarInfo(ctx, req.LicencePlate)

	if err != nil {
		return nil, handleError(err)
	}

	return &pb.CarInfo{
		LicencePlate:      carInfo.LicencePlate,
		Model:             carInfo.Model,
		YearOfManufacture: carInfo.YearOfManufacture,
		Type:              carInfo.TypeCar.String(),
	}, nil
}

func (s *CarServer) GetAllCarInfo(ctx context.Context, _ *pb.Empty) (*pb.CarInfoList, error) {
	carInfoList, err := s.carService.GetAllCarInfo(ctx)

	if err != nil {
		return nil, handleError(err)
	}

	res := make([]*pb.CarInfo, len(carInfoList))

	for i := 0; i < len(res); i++ {
		res[i] = &pb.CarInfo{
			LicencePlate:      carInfoList[i].LicencePlate,
			Model:             carInfoList[i].Model,
			YearOfManufacture: carInfoList[i].YearOfManufacture,
			Type:              carInfoList[i].TypeCar.String(),
		}
	}

	return &pb.CarInfoList{CarInfoList: res}, nil
}

func handleError(err error) error {
	if myErr, ok := err.(*customError.Error); ok {
		switch myErr.Code() {
		case customError.BadRequest:
			return status.Error(codes.InvalidArgument, myErr.Message())
		case customError.NotFound:
			return status.Error(codes.NotFound, myErr.Message())
		case customError.Conflict:
			return status.Error(codes.InvalidArgument, myErr.Message())
		case customError.Internal:
			return status.Error(codes.Internal, "Something wrong")
		default:
			return status.Error(codes.InvalidArgument, "Something wrong")
		}
	} else {
		return status.Error(codes.InvalidArgument, "Something wrong")
	}
}

func validation(s interface{}) error {
	validate := validator.New()
	if err := validate.Struct(s); err != nil {
		var validationErrors []string
		for _, err := range err.(validator.ValidationErrors) {
			validationErrors = append(validationErrors, fmt.Sprintf("%s validation failed on %s", err.Tag(), err.Field()))
		}
		return status.Errorf(codes.InvalidArgument, "validation failed: %v", validationErrors)
	}
	return nil
}
