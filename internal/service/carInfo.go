package service

import (
	"Fleet_Management_System/internal/customError"
	"Fleet_Management_System/internal/model"
	"context"
	"go.uber.org/zap"
)

type CarRepository interface {
	CreateCar(context.Context, *model.Car) (*model.Car, error)
	GetCar(context.Context, string) (*model.Car, error)
	GetCarInfo(context.Context, string) (*model.CarInfo, error)
	CreateCarInfo(context.Context, *model.CarInfo) (*model.CarInfo, error)
	UpdateCarInfo(context.Context, *model.CarInfo) (*model.CarInfo, error)
	DeleteCarInfo(context.Context, string) (*model.CarInfo, error)
	GetAllCarInfo(context.Context) ([]*model.CarInfo, error)
}

type CarService struct {
	repository CarRepository
	logger     *zap.SugaredLogger
}

func NewCarService(repository CarRepository, logger *zap.SugaredLogger) *CarService {
	return &CarService{
		repository: repository,
		logger:     logger,
	}
}

func (s *CarService) GetCarInfo(ctx context.Context, licencePlate string) (*model.CarInfo, error) {
	carInfo, err := s.repository.GetCarInfo(ctx, licencePlate)

	if err != nil {
		s.log(err)
		return nil, err
	}

	return carInfo, nil
}

func (s *CarService) CreateCarInfo(ctx context.Context, carInfo *model.CarInfo) (*model.CarInfo, error) {
	carInfo, err := s.repository.CreateCarInfo(ctx, carInfo)

	if err != nil {
		s.log(err)
		return nil, err
	}

	return carInfo, nil
}

func (s *CarService) UpdateCarInfo(ctx context.Context, carInfo *model.CarInfo) (*model.CarInfo, error) {
	carInfo, err := s.repository.UpdateCarInfo(ctx, carInfo)

	if err != nil {
		s.log(err)
		return nil, err
	}

	return carInfo, nil
}

func (s *CarService) DeleteCarInfo(ctx context.Context, licencePlate string) (*model.CarInfo, error) {
	carInfo, err := s.repository.DeleteCarInfo(ctx, licencePlate)

	if err != nil {
		s.log(err)
		return nil, err
	}

	return carInfo, nil
}

func (s *CarService) GetAllCarInfo(ctx context.Context) ([]*model.CarInfo, error) {
	carInfoList, err := s.repository.GetAllCarInfo(ctx)

	if err != nil {
		s.log(err)
		return nil, err
	}

	return carInfoList, nil
}

func (s *CarService) RegistrationNewCar(ctx context.Context, car *model.Car) (*model.Car, error) {
	car, err := s.repository.CreateCar(ctx, car)

	if err != nil {
		s.log(err)
		return nil, err
	}

	return car, nil
}
func (s *CarService) GetCarMileage(ctx context.Context, licencePlate string) (int32, error) {
	car, err := s.repository.GetCar(ctx, licencePlate)

	if err != nil {
		s.log(err)
		return 0, err
	}

	return car.Mileage, nil
}
func (s *CarService) GetCostOfCar(ctx context.Context, licencePlate string) (int32, error) {
	car, err := s.repository.GetCar(ctx, licencePlate)

	if err != nil {
		s.log(err)
		return 0, err
	}
	var cost int32 = 0
	if car.Mileage != 0 {
		cost = (car.Consumption*car.Mileage + car.Tax) / car.Mileage
	}

	return cost, nil
}

func (s *CarService) log(err error) {
	if myErr, ok := err.(*customError.Error); ok {
		s.logger.Error(myErr.StackTrace())
	} else {
		s.logger.Error(err)
	}
}
