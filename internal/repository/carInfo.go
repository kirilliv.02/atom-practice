package repository

import (
	"Fleet_Management_System/internal/customError"
	"Fleet_Management_System/internal/model"
	"context"
	"github.com/jackc/pgx/v5/pgxpool"
)

type CarRepository struct {
	pool *pgxpool.Pool
}

func NewCarRepository(db *pgxpool.Pool) *CarRepository {
	return &CarRepository{pool: db}
}

func (r *CarRepository) GetCarInfo(ctx context.Context, licencePlate string) (*model.CarInfo, error) {
	row := r.pool.QueryRow(ctx, "SELECT id, licence_plate, model, year_of_Manufacture, type_car FROM car_info WHERE licence_plate = $1;", licencePlate)

	carInfo := &model.CarInfo{}
	err := row.Scan(&carInfo.Id, &carInfo.LicencePlate, &carInfo.Model, &carInfo.YearOfManufacture, &carInfo.TypeCar)

	if err != nil {
		return nil, customError.Wrap("CarRepository", "GetCarInfo", "licence plate not found", customError.NotFound, err)
	}

	return carInfo, nil
}

func (r *CarRepository) CreateCarInfo(ctx context.Context, carInfo *model.CarInfo) (*model.CarInfo, error) {
	row := r.pool.QueryRow(ctx, "SELECT id FROM car_info WHERE licence_plate = $1", carInfo.LicencePlate)

	err := row.Scan(&carInfo.Id)

	if err == nil {
		return nil, customError.New("CarRepository", "CreateCarInfo", "such car info already exist", customError.Conflict)
	}

	row = r.pool.QueryRow(ctx, "INSERT INTO car_info (licence_plate, model, year_of_Manufacture, type_car) VALUES ($1, $2, $3, $4) RETURNING id;",
		carInfo.LicencePlate, carInfo.Model, carInfo.YearOfManufacture, carInfo.TypeCar)

	err = row.Scan(&carInfo.Id)

	if err != nil {
		return nil, customError.Wrap("CarRepository", "CreateCarInfo", "something gone wrong", customError.BadRequest, err)
	}
	return carInfo, nil

}
func (r *CarRepository) UpdateCarInfo(ctx context.Context, carInfo *model.CarInfo) (*model.CarInfo, error) {
	row := r.pool.QueryRow(ctx, "SELECT id FROM car_info WHERE licence_plate = $1", carInfo.LicencePlate)

	err := row.Scan(&carInfo.Id)

	if err != nil {
		return nil, customError.Wrap("CarRepository", "UpdateCarInfo", "licence plate not found", customError.NotFound, err)
	}

	row = r.pool.QueryRow(ctx, "UPDATE car_info SET model = $1, year_of_manufacture = $2, type_car = $3 WHERE licence_plate= $4 RETURNING id;",
		carInfo.Model, carInfo.YearOfManufacture, carInfo.TypeCar.String(), carInfo.LicencePlate)

	err = row.Scan(&carInfo.Id)

	if err != nil {
		return nil, customError.Wrap("CarRepository", "UpdateCarInfo", "something gone wrong", customError.BadRequest, err)
	}
	return carInfo, nil
}

func (r *CarRepository) DeleteCarInfo(ctx context.Context, licencePlate string) (*model.CarInfo, error) {
	row := r.pool.QueryRow(ctx, "DELETE FROM car_info WHERE licence_plate = $1 RETURNING id, licence_plate, model, year_of_manufacture, type_car;", licencePlate)

	carInfo := &model.CarInfo{}

	err := row.Scan(&carInfo.Id, &carInfo.LicencePlate, &carInfo.Model, &carInfo.YearOfManufacture, &carInfo.TypeCar)

	if err != nil {
		return nil, customError.Wrap("CarRepository", "DeleteCarInfo", "licence plate not found", customError.NotFound, err)
	}
	return carInfo, nil
}

func (r *CarRepository) GetAllCarInfo(ctx context.Context) ([]*model.CarInfo, error) {
	rows, err := r.pool.Query(ctx, "SELECT id, licence_plate, model, year_of_Manufacture, type_car FROM car_info;")
	defer rows.Close()

	if err != nil {
		return nil, customError.Wrap("CarRepository", "GetAllCarInfo", "car infos not found", customError.NotFound, err)
	}

	carInfoList := make([]*model.CarInfo, 0, 10)

	for rows.Next() {
		carInfo := &model.CarInfo{}

		err = rows.Scan(&carInfo.Id, &carInfo.LicencePlate, &carInfo.Model, &carInfo.YearOfManufacture, &carInfo.TypeCar)

		if err != nil {
			break
		}

		carInfoList = append(carInfoList, carInfo)
	}

	return carInfoList, nil
}

func (r *CarRepository) CreateCar(ctx context.Context, car *model.Car) (*model.Car, error) {
	row := r.pool.QueryRow(ctx, "SELECT id FROM car WHERE licence_plate = $1", car.LicencePlate)

	err := row.Scan(&car.Id)

	if err == nil {
		return nil, customError.New("CarRepository", "CreateCar", "such car already exist", customError.Conflict)
	}

	row = r.pool.QueryRow(ctx, "INSERT INTO car (licence_plate, mileage, consumption, tax) VALUES ($1, $2, $3, $4) RETURNING id;",
		car.LicencePlate, car.Mileage, car.Consumption, car.Tax)

	err = row.Scan(&car.Id)

	if err != nil {
		return nil, customError.Wrap("CarRepository", "CreateCar", "something gone wrong", customError.BadRequest, err)
	}
	return car, nil
}
func (r *CarRepository) GetCar(ctx context.Context, licencePlate string) (*model.Car, error) {
	row := r.pool.QueryRow(ctx, "SELECT id, licence_plate, mileage, consumption, tax FROM car WHERE licence_plate = $1;",
		licencePlate)

	car := &model.Car{}

	err := row.Scan(&car.Id, &car.LicencePlate, &car.Mileage, &car.Consumption, &car.Tax)

	if err != nil {
		return nil, customError.Wrap("CarRepository", "GetCar", "licence plate not found", customError.NotFound, err)
	}
	return car, nil
}
