package client

import (
	"Fleet_Management_System/internal/config"
	pb "Fleet_Management_System/proto"
	"bufio"
	"context"
	"fmt"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"
)

func Run() {
	in := bufio.NewReader(os.Stdin)

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)
	defer cancel()

	if err := setupViper(); err != nil {
		log.Panic(err)
	}

	conn, err := config.SetupClient()
	defer closeConnection(conn)

	if err != nil {
		log.Fatalf(err.Error())
	}

	client := pb.NewCarInfoServiceClient(conn)

loop:
	for {
		select {
		case <-ctx.Done():
			break loop
		default:
			break
		}

		fmt.Println("1. Зарегистрировать автомобиль")
		fmt.Println("2. Добавить инормацию о машине")
		fmt.Println("3. Посмотреть информацию о машине")
		fmt.Println("4. Обновить инормацию о машине")
		fmt.Println("5. Удалить инормацию о машине")
		fmt.Println("6. Посмотреть информацию по всем машинам")
		fmt.Println("7. Отследить пробег автомобиля")
		fmt.Println("8. Стоимость использования автомобиля (за 1 км)")
		fmt.Println("9. Выйти")
		chosen := readInt64(in)

		clearConsole()

		switch chosen {
		case 1:
			fmt.Print("Введите номер автомобиля (в443уе16):")
			licencePlate := readLine(in)
			fmt.Print("Введите пробег (в км):")
			mileage := readInt64(in)
			fmt.Print("Введите расход (на 100 км):")
			consumption := readInt64(in)
			fmt.Print("Введите стоимость ежегодного налога (в руб):")
			tax := readInt64(in)
			carReq := &pb.Car{
				LicencePlate: licencePlate,
				Mileage:      int32(mileage),
				Consumption:  int32(consumption),
				Tax:          int32(tax),
			}

			car, err := client.RegistrationNewCar(ctx, carReq)
			if err != nil {
				fmt.Println(err)
				break
			}
			printCar(car)

		case 2:
			fmt.Print("Введите номер автомобиля (в443уе16):")
			licencePlate := readLine(in)
			fmt.Print("Введите модель автомобиля:")
			model := readLine(in)
			fmt.Print("Введите год производства автомобиля:")
			year := readInt64(in)
			fmt.Print("Введите тип автомобиля (Taxi, Delivery, Carsharing):")
			typeCar := readLine(in)

			carInfoRequest := &pb.CarInfo{
				LicencePlate:      licencePlate,
				Model:             model,
				YearOfManufacture: int32(year),
				Type:              typeCar,
			}

			carInfo, err := client.CreateCarInfo(ctx, carInfoRequest)
			if err != nil {
				fmt.Println(err.Error())
				break
			}
			printCarInfo(carInfo)
		case 3:
			fmt.Print("Введите номер автомобиля (в443уе16):")
			licencePlate := readLine(in)
			carInfo, err := client.GetCarInfo(ctx, &pb.CarInfoRequest{LicencePlate: licencePlate})
			if err != nil {
				fmt.Println(err.Error())
				break
			}
			printCarInfo(carInfo)
		case 4:
			fmt.Print("Введите номер автомобиля (в443уе16):")
			licencePlate := readLine(in)
			fmt.Print("Введите модель автомобиля:")
			model := readLine(in)
			fmt.Print("Введите год производства автомобиля:")
			year := readInt64(in)
			fmt.Print("Введите тип автомобиля (Taxi, Delivery, Carsharing):")
			typeCar := readLine(in)

			carInfoReq := &pb.CarInfo{
				LicencePlate:      licencePlate,
				Model:             model,
				YearOfManufacture: int32(year),
				Type:              typeCar,
			}

			carInfo, err := client.UpdateCarInfo(ctx, carInfoReq)
			if err != nil {
				fmt.Println(err)
				break
			}
			printCarInfo(carInfo)
		case 5:
			fmt.Print("Введите номер автомобиля (в443уе16):")
			licencePlate := readLine(in)
			carInfo, err := client.DeleteCarInfo(ctx, &pb.CarInfoRequest{LicencePlate: licencePlate})
			if err != nil {
				fmt.Println(err)
				break
			}
			fmt.Println("Информация об автомобиле удалена!")
			printCarInfo(carInfo)
		case 6:
			carInfoList, err := client.GetAllCarInfo(ctx, &pb.Empty{})
			if err != nil {
				fmt.Println(err)
				break
			}
			printCarInfoList(carInfoList)
		case 7:
			fmt.Print("Введите номер автомобиля (в443уе16):")
			licencePlate := readLine(in)
			mileage, err := client.GetCarMileage(ctx, &pb.CarInfoRequest{LicencePlate: licencePlate})
			if err != nil {
				fmt.Println(err)
				break
			}
			fmt.Printf("Пробег автомобиля с номером: %s - %d км\n", licencePlate, mileage.Mileage)
		case 8:
			fmt.Print("Введите номер автомобиля (в443уе16): ")
			licencePlate := readLine(in)
			cost, err := client.GetCostOfCar(ctx, &pb.CarInfoRequest{LicencePlate: licencePlate})
			if err != nil {
				fmt.Println(err)
				break
			}
			fmt.Printf("Расход автомобиля с номером: %s за 1 км - %d руб\n", licencePlate, cost.CostOfUse)
		case 9:
			fmt.Println("До свидания!")
			break loop
		case -1:
			fmt.Println("Введите число! Попробуйте еще раз...")
		default:
			fmt.Println("Неверное число! Попробуйте еще раз...")
		}
	}

}

func readLine(in *bufio.Reader) string {
	s, _ := in.ReadString('\n')
	s = strings.Replace(s, "\n", "", -1)
	s = strings.Replace(s, "\r", "", -1)
	return s
}

func readInt64(in *bufio.Reader) int64 {
	s := readLine(in)
	n, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return -1
	}
	return n
}

func setupViper() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		return err
	}
	return nil
}

func printCar(car *pb.Car) {
	fmt.Println("Номер автомобиля:", car.LicencePlate)
	fmt.Println("Расход автомобиля (на 100 км):", car.Consumption)
	fmt.Println("Пробег автомобиля:", car.Mileage)
	fmt.Println("Налог автомобиля:", car.Tax)

}

func printCarInfoList(list *pb.CarInfoList) {
	for i := 0; i < len(list.CarInfoList); i++ {
		fmt.Printf("%d. ", i+1)
		printCarInfo(list.CarInfoList[i])
	}
}

func printCarInfo(carInfo *pb.CarInfo) {
	fmt.Println("Номер автомобиля:", carInfo.LicencePlate)
	fmt.Println("Модель автомобиля:", carInfo.Model)
	fmt.Println("Год производства автомобиля:", carInfo.YearOfManufacture)
	fmt.Println("Тип автомобиля:", carInfo.Type)
}

func clearConsole() {
	var cmd *exec.Cmd
	switch runtime.GOOS {
	case "windows":
		cmd = exec.Command("cmd", "/c", "cls")
	default:
		cmd = exec.Command("clear")
	}
	cmd.Stdout = os.Stdout
	err := cmd.Run()
	if err != nil {
		fmt.Println("error clear")
	}
}

func closeConnection(conn *grpc.ClientConn) {
	err := conn.Close()
	if err != nil {
		fmt.Println("Failed to close connection")
	}
}
