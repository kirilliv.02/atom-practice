package server

import (
	"Fleet_Management_System/internal/config"
	"Fleet_Management_System/internal/customError"
	"Fleet_Management_System/internal/repository"
	"Fleet_Management_System/internal/server"
	"Fleet_Management_System/internal/service"
	pb "Fleet_Management_System/proto"
	"context"
	"fmt"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func Run() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)
	defer cancel()

	logger, err := loggerInit()
	if err != nil {
		_ = fmt.Errorf("failed start logger: %w", err)
		return
	}

	if err = setupViper(); err != nil {
		logger.Error(err)
		return
	}

	pool, err := config.SetupPostgres(ctx)
	if err != nil {
		logger.Error(err)
		return
	}

	carRepository := repository.NewCarRepository(pool)
	carService := service.NewCarService(carRepository, logger)
	carServer := server.NewCarServer(carService)

	grpcServer, addr, err := config.SetupGrpc()
	if err != nil {
		logger.Error(err)
		return
	}

	lis, err := net.Listen("tcp", addr)
	if err != nil {
		logger.Error(err)
		return
	}

	pb.RegisterCarInfoServiceServer(grpcServer, carServer)

	fmt.Printf("server gRPC started at %v\n", addr)

	if err = grpcServer.Serve(lis); err != nil {
		err = customError.Wrap("app", "Run", "failed starting gRPC server", customError.Internal, err)
		logger.Error(err)
		return
	}
}

func setupViper() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		return customError.Wrap("app", "setupViper", "failed read config", customError.Internal, err)
	}

	return nil
}

func loggerInit() (*zap.SugaredLogger, error) {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.TimeEncoderOfLayout(time.RFC1123Z)

	fileEncoder := zapcore.NewJSONEncoder(encoderConfig)

	file, err := os.OpenFile("./logs/logs.json", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)

	if err != nil {
		return nil, customError.Wrap("app", "loggerInit", "failed open file", customError.Internal, err)
	}

	writeSyncer := zapcore.AddSync(file)
	defaultLogLevel := zapcore.ErrorLevel
	core := zapcore.NewTee(
		zapcore.NewCore(fileEncoder, writeSyncer, defaultLogLevel),
	)

	logger := zap.New(core).Sugar()

	return logger, nil
}
