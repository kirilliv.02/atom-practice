package customError

import (
	"fmt"
	"strings"
)

type Error struct {
	_struct string
	fun     string
	msg     string
	err     error
	code    Code
}

func (e *Error) Message() string {
	return e.msg
}

func (e *Error) Code() Code {
	return e.code
}

func (e *Error) Error() string {
	return fmt.Sprintf("%s failed with status code %d in %s: %s", e.fun, e.code, e._struct, e.msg)
}

func (e *Error) StackTrace() string {
	sb := &strings.Builder{}
	sb.WriteString(e.Error())
	err := e.err

	for err != nil {
		sb.WriteString(", ")
		sb.WriteString(err.Error())
		if myErr, ok := err.(*Error); ok {
			err = myErr.err
		} else {
			err = nil
		}
	}

	return sb.String()
}

func Wrap(_struct, fun, msg string, code Code, err error) error {
	return &Error{
		_struct: _struct,
		fun:     fun,
		msg:     msg,
		err:     err,
		code:    code,
	}
}

func (e *Error) Unwrap() error {
	return e.err
}

func New(_struct, fun, msg string, code Code) error {
	return &Error{
		_struct: _struct,
		fun:     fun,
		msg:     msg,
		code:    code,
	}
}

type Code uint32

const (
	BadRequest Code = 400
	NotFound   Code = 404
	Conflict   Code = 409
	Internal   Code = 500
)
