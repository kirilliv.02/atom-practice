package model

import "strings"

type CarInfo struct {
	Id                int64
	LicencePlate      string  `validate:"required,min=5"`
	Model             string  `validate:"required"`
	YearOfManufacture int32   `validate:"required,gte=2000"`
	TypeCar           typeCar `validate:"required"`
}

func (t typeCar) String() string {
	switch t {
	case CarSharing:
		return "CARSHARING"
	case Taxi:
		return "TAXI"
	case Delivery:
		return "DELIVERY"
	default:
		return "UNDEFINED"
	}
}

func parseTypeCar(t string) typeCar {
	t = strings.ToUpper(t)
	switch t {
	case CarSharing.String():
		return CarSharing
	case Taxi.String():
		return Taxi
	case Delivery.String():
		return Delivery
	default:
		return undefined
	}
}

func (c *CarInfo) SetTypeCar(t string) {
	c.TypeCar = parseTypeCar(t)
}

type typeCar int32

const (
	undefined typeCar = iota
	CarSharing
	Taxi
	Delivery
)
