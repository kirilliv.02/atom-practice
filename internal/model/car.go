package model

type Car struct {
	Id           int64
	LicencePlate string `validate:"required,min=5"`
	Mileage      int32  `validate:"required,gte=0"`
	Consumption  int32  `validate:"required,gte=1"`
	Tax          int32  `validate:"required,gte=0"`
}
